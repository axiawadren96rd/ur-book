angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('tabsController.login', {
    url: '/page5',
    views: {
      'tab1': {
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      }
    }
  })

  .state('tabsController.signup', {
    url: '/page6',
    views: {
      'tab3': {
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
      }
    }
  })

  .state('allitem', {
      url: '/page7',
      templateUrl: 'templates/allitem.html',
      controller: 'menu1Ctrl'
    })

 .state('menu2', {
      url: '/page8',
      templateUrl: 'templates/menu2.html',
      controller: 'menu1Ctrl'
    })

 .state('menu3', {
      url: '/page9',
      templateUrl: 'templates/menu3.html',
      controller: 'menu1Ctrl'
    })

	.state('menu4', {
      url: '/page10',
      templateUrl: 'templates/menu4.html',
      controller: 'menu1Ctrl'
    })
		
	.state('IT', {
      url: '/page12',
      templateUrl: 'templates/IT.html',
      controller: 'menu1Ctrl'
    })
	
		.state('selection', {
      url: '/page13',
      templateUrl: 'templates/selection.html',
      controller: 'menu1Ctrl'
    })

 
  .state('myCart', {
    url: '/page14',
    templateUrl: 'templates/myCart.html',
    controller: 'myCartCtrl'
  })

  .state('lastOrders', {
    url: '/page15',
    templateUrl: 'templates/lastOrders.html',
    controller: 'lastOrdersCtrl'
  })

  .state('settings', {
    url: '/page16',
    templateUrl: 'templates/settings.html',
    controller: 'settingsCtrl'
  })

  .state('support', {
    url: '/page17',
    templateUrl: 'templates/support.html',
    controller: 'supportCtrl'
  })

  .state('checkout', {
    url: '/page18',
    templateUrl: 'templates/checkout.html',
    controller: 'checkoutCtrl'
  })

 

$urlRouterProvider.otherwise('/page1/page5')



});
